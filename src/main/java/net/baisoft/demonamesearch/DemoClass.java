package net.baisoft.demonamesearch;

import java.util.ArrayList;
import java.util.List;

import net.baisoft.namesearch.NameSearcher;
import net.baisoft.namesearch.po.Name;

/**
 * 演示程序
 *
 * @author zhaozhantao
 *
 */
class DemoClass {
    private DemoClass() {
    }
    public static void main(String[] args) {

        NameSearcher nameSearcher = new NameSearcher();

        //创建一组姓名
        List<Name> names = new ArrayList<Name>();
        Name name1 = new Name();
        name1.setKey("1");
        name1.setValue("赵占涛");
        names.add(name1);
        Name name2 = new Name();
        name2.setKey("2");
        name2.setValue("张三");
        names.add(name2);

        //设置一组姓名到搜索器中
        nameSearcher.setPeoples(names);

        List<Name> results = nameSearcher.searchPeoples("ztao");//搜索占涛
        if (!results.isEmpty()) {
            for (Name name : results) {
                System.out.println(name.getKey() + ":" + name.getValue());
            }
        }
    }
}
