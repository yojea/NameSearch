package net.baisoft.namesearch.po;

/**
 * name<br/>
 * the master model class of NameSearch e.g. book name, people name, article name
 * 名称搜索的主体对象，例如：书名，人名，文章名
 *
 * @author zhaozhantao@qq.com
 *
 */
public class Name {


    /**
     * key<br>
     *     normally it should be TABLE RECORD ID
     *     一般来说，它应该设为表记录的id
     */
    private String key;

    /**
     * value<br>
     *     value of search object
     *     被查询对象的值
     */
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
